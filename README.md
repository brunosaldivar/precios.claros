##Precios Claros

Obtiene y persiste precios de productos según sucursal y categoría

## Instalación

Luego de descargar el repositorio, ejecutar

1. npm install
2. node main.js

## Modo de uso

node main.js -csv 
Exporta archivo .csv de sucursales a SQL\r\n'

node main.js -procesar
Obtiene precios de API y persiste\r\n'

node main.js -delete tabla
Elimina datos de tabla.
'tabla' puede ser: sucursales o productos

node main.js -importar
Obtiene precios de SQL Azure y guarda en archivo csv (próximamente)

## Configuración

Ver archivo config.js