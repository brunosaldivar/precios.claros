//Iantech2018

exports.Config =(function (){

    return {
        
        db  :   {
            userName    : 'sa', 
            password    : 'Iantech2018#', 
            server      : '209.50.52.132', 
             options: {
                database: 'PreciosClaros2',
                encrypt : true
            } 
        },
        jobHour                :    2, //2 am
        jobMinutes             :    0,
        sequentialCount        :   3,   // cantidad de veces que ejecuta "parallelProcess"
        debug                  :   true,
        parallel               :   true,
        parallelProcess        :   3,
        timeToSleep     :   1000, //seconds
        url : {
            productos   :   'https://d3e6htiiul5ek9.cloudfront.net/prod/productos',
            sucursales  :   'https://d3e6htiiul5ek9.cloudfront.net/prod/sucursales',
            categorias  :   'https://d3e6htiiul5ek9.cloudfront.net/prod/categorias'
        },
        dbFiles :{
            CREATE_TABLE_pc_sucursales_compare: './scripts/CREATE_TABLE_pc_sucursales_compare.sql',
            CREATE_PROCEDURE_pc_sucursales_sync: './scripts/CREATE_PROCEDURE_pc_sucursales_sync.sql',
            CREATE_TABLE_pc_sucursales: './scripts/CREATE_TABLE_pc_sucursales.sql',
            CREATE_TABLE_pc_categorias: './scripts/CREATE_TABLE_pc_categorias.sql',
            CREATE_TABLE_pc_productos: './scripts/CREATE_TABLE_pc_productos.sql',
            CREATE_TABLE_pc_productos_history: './scripts/CREATE_TABLE_pc_productos_history.sql',
            INSERT_INTO_pc_productos_history: './scripts/INSERT_INTO_pc_productos_history.sql',
            INSERT_pc_categorias: './scripts/INSERT_pc_categorias.sql',
        },
        key             :   'zIgFou7Gta7g87VFGL9dZ4BEEs19gNYS1SOQZt96',
        
        fileCSVSucursales  :   './csv/sucursales.csv',

        modoUso         :   'Iantech -Precios Claros-\r\nObtiene precios de productos, según categoría, de API preciosclaros.gob.ar y persiste en SQL.\r\nModo de uso: \r\n'
        + '-h o -help:  Este mensaje\r\n'
        + '-csv: Exporta archivo .csv de sucursales a SQL\r\n'
        + '-i: Crea tablas y datos necesarios para operar\r\n'
        + '-a [categoria]: procesa [categoria]\r\n'  
        + '-p: procesa categorias en paralelo\r\n', 
    };

})();
