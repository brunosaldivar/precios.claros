    const   models = require('./models').models,
        common = require('./common/').commons,
        config = require('./config').Config,
        managers = require('./managers/').managers;

exports.bootstrap = (() => {
    
    initSucursales  = () => {
        return new Promise(function(resolve, reject){
            common.logger.log({msg: '[init] sucursales.createTable()'})

            models.sucursales.createTable().then(()=>{
                common.logger.log({msg: '[init] sucursales_compare.createTable()'})
                models.sucursales_compare.createTable().then(()=>{
                    models.sucursales.createProcedureSucursalesSync().then(()=>{
                        resolve();
                    }).catch((error)=>{
                        common.logger.log({msg: 'error sucursales.createProcedureSucursalesSync()'});
                        //common.logger.debug({data: error}, true);
                        reject(error)
                    });      
                }).catch((error)=>{
                    common.logger.log({msg: 'error sucursales_compare.createTable()'});
                    //common.logger.debug({data: error}, true);
                    reject(error)
                });
            }).catch((error)=>{
                common.logger.log({msg: 'error sucursales.createTable()'});
                //common.logger.debug({data: error}, true);
                reject(error)
            });
        });
    };
    seedSucursales  = () => {
        return new Promise(function(resolve, reject){

            models.sucursales.truncate().then((codes)=>{
                models.sucursales_compare.truncate().then((codes)=>{

                    common.logger.log({msg: '[seed] sucursales.procesar()..'})
                    managers.sucursales.procesar().then(()=>{
                        
                        managers.sucursales.CSVSync().then(()=> {
                            resolve();
                        }).catch((error)=>{
                            common.logger.log({msg: 'error sucursales.CSVSync()'});
                            common.logger.debug({data: error}, true);
                            reject(error)
                        });

                    }).catch((error)=>{
                        common.logger.log({msg: 'error sucursales.procesar()'});
                        common.logger.debug({data: error}, true);
                        reject(error)
                    });
            }).catch((error)=>{    
                common.logger.log({msg: 'error sucursales_compare.truncate()'});
                common.logger.debug({data: error}, true);
                reject(error)
            });
        }).catch((error)=>{
                common.logger.log({msg: 'error sucursales.truncate()'});
                common.logger.debug({data: error}, true);
                reject(error)
        });
    })};
    initCategorias        =   ()    =>  {

        return new Promise(function(resolve, reject){
            common.logger.log({msg : '[init] create table pc_categorias'});
            models.categorias.createTable().then(()=>{

                common.logger.log({msg : '[seed] pc_categorias'});

                models.categorias.seed().then(()=>{
                    resolve();
                }).catch((error)=>{
                    common.logger.log({msg: 'error categorias.seed()'});
                    //common.logger.debug({data: error}, true);
                    reject(error)
                });
            }).catch((error)=>{
                common.logger.log({msg: 'error categorias.createTable()'});
               // common.logger.debug({data: error}, true);
                reject(error)
            });
        });
    };
    initProductos        =   () =>  {

        return new Promise(function(resolve, reject){
            common.logger.log({msg : '[init] create table pc_productos with index..'});
            models.productos.createTable().then(()=>{
                common.logger.log({msg : '[init] create table pc_productos_history with index..'});
                models.productos.createTableHistory().then(resolve).catch((error)=>{
                    common.logger.log({msg: 'error productos.createTableHistory()'});
                    reject(error)
                });
                
            }).catch((error)=>{
                common.logger.log({msg: 'error productos.createTable()'});
                reject(error)
            });
        });
    };
    
    return {
       
        init        :   function(){

            return new Promise(function(resolve, reject){
                initSucursales().then(() => {
                    initProductos().then(() => {
                        initCategorias().then(() => {
                            seedSucursales().then(() => {
                                resolve();
                            }).catch(reject)
                        }).catch(reject);
                    }).catch(reject);
                }).catch(reject);
            });
        },
    }

})();
