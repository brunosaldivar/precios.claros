const   sucursales = require('./sucursales').sucursales,
        sucursales_compare = require('./sucursales_compare').sucursales_compare,
        productos = require('./productos').productos,
        categorias = require('./categorias').categorias;

exports.models = {productos: productos, sucursales: sucursales, categorias: categorias,
    sucursales_compare: sucursales_compare};