const   config = require('./../config.js').Config,
        common = require('./../common').commons,
        TYPES = common.types,
        request = require('request'),
        fcsv = require("fast-csv");

exports.sucursales =(function (){
    
    return {
        total : {},

        insertMasivo : function (sucursales){
            return new Promise(function(resolve, reject){

                common.sqls.connect().then(function(connection){
                    var bulkLoad = connection.newBulkLoad('dbo.pc_Sucursales', function (error, rowCount) {
                        if(error){
                            common.logger.log({data : error} ,true);
                            connection.close();
                            reject(error)
                        }else{
                            common.logger.log({msg: rowCount + ' sucursales insertadas' });
                            connection.close();
                            resolve();
                        }
                    });
                    bulkLoad.addColumn('code', TYPES.NVarChar, { length:50, nullable: false});
                    bulkLoad.addColumn('banderaId', TYPES.Int, {  nullable: false});
                    bulkLoad.addColumn('sucursalNombre', TYPES.NVarChar, { length: 100, nullable: false});
                    bulkLoad.addColumn('sucursalTipo', TYPES.NVarChar, { length: 100, nullable: false});
                    bulkLoad.addColumn('provincia', TYPES.NVarChar, { length: 50, nullable: false});
                    bulkLoad.addColumn('direccion', TYPES.NVarChar, { length: 150, nullable: false});
                    bulkLoad.addColumn('banderaDescripcion', TYPES.NVarChar, { length: 150, nullable: false});
                    bulkLoad.addColumn('localidad', TYPES.NVarChar, { length: 100, nullable:false});
                    bulkLoad.addColumn('comercioRazonSocial', TYPES.NVarChar, { length:150, nullable:false});
                    bulkLoad.addColumn('comercioId', TYPES.Int, { nullable: false});
                    bulkLoad.addColumn('sucursalId', TYPES.Int, { nullable: false});
                    

                    for (i = 0; i <= sucursales.length ; i++){
                        if(sucursales[i]){
                            sucursales[i].code = sucursales[i].id;
                            delete sucursales[i].id;
                            sucursales[i].fechaAlta = new Date();
                            bulkLoad.addRow(sucursales[i]);
                        }
                    }
                    connection.execBulkLoad(bulkLoad);
                    
                    }).catch(reject);
            });
        },
        obtenerSucursales : function (opt){
            return new Promise(function(resolve, reject){
                try{
                    var options = {
                    url: config.url.sucursales,
                    method: 'GET',
                    qs: 
                    { 
                        offset: opt.offset,
                        limit: opt.limit,
                    },
                    headers: {
                        'x-api-key':config.key,
                    },
                    };
                    //common.logger.log({data:options}, true);
                    request(options, function(error,response, html){
                        
                        if(response.statusCode && response.statusCode !== 200){
                            common.logger.log({msg: response.request.uri.href})
                            common.logger.log({msg:'statusCode: ' + response.statusCode});
                        }
                        //common.logger.log({data:response}, true);
                        if (!error){ 
                            resolve(response);
                        }else{
                            common.logger.log({data: error}, true);
                            reject(error);
                        }}); 
                    }catch(ex) {
                        reject(ex);
                    }
            });
        },
        createProcedureSucursalesSync  : function(){

            return new Promise(function(resolve, reject){
                
                common.files.open(config.dbFiles.CREATE_PROCEDURE_pc_sucursales_sync).then(function(query){
                    common.sqls.ejecutarQuery(query).then(function(result) {
                        resolve()
                    }).catch(function(error) {
                        common.logger.debug({data: error}, true);
                        reject(error)
                    });
                }).catch(function(error) {
                    common.logger.debug({data: error}, true);
                    reject(error)
                });
                
            });
        },
        
        createTable  : function(){

            return new Promise(function(resolve, reject){
                
                common.files.open(config.dbFiles.CREATE_TABLE_pc_sucursales).then(function(query){
                    common.sqls.ejecutarQuery(query).then(function(result) {
                        resolve()
                    }).catch(function(error) {
                        common.logger.debug({data: error}, true);
                        reject(error)
                    });
                }).catch(function(error) {
                    common.logger.debug({data: error}, true);
                    reject(error)
                });
                
            });
        },

        truncate : function(){
            return new Promise(function(resolve, reject){
                 
                var query = "TRUNCATE TABLE pc_sucursales";
                common.sqls.ejecutarQuery(query).then(function(result) {
                    resolve(result)
                }).catch(function(error) {
                    common.logger.debug({data: error}, true);
                    reject(error)
                });
            });
           
        },
        insertMasivoSucursales2 : function (sucursales){
            return new Promise(function(resolve, reject){
                common.sqls.connect().then(function(connection){

                var bulkLoad = connection.newBulkLoad('dbo.Sucursales', function (error, rowCount) {
                    if(error){
                        console.log(error);
                        connection.close();
                    }else{
                        console.log('%d sucursales insertadas', rowCount);
                        connection.close();
                    }
                });
                
                bulkLoad.addColumn('Code', TYPES.NVarChar, { length: 50, nullable: false});
                bulkLoad.addColumn('Name', TYPES.NVarChar, { length: 50, nullable: false });
                bulkLoad.addColumn('TradeName', TYPES.NVarChar, { length: 50, nullable: false });
                //console.log(sucursales);
                for (i = 0; i <= sucursales.length ; i++){
                    if(sucursales[i])
                        bulkLoad.addRow(sucursales[i]);
                }
                connection.execBulkLoad(bulkLoad);
                }).catch(reject);
             });
            
        },
        getSucursalesDB : function(){
            return new Promise(function(resolve, reject){

                let query = "SELECT s.code, s.sucursalNombre " +
                            "FROM pc_sucursales s " +
                            "WHERE isExcluded = 0 " +
                            "ORDER BY s.id DESC";

                common.sqls.ejecutarQuery(query).then(function(result) {
                    resolve(result)
                }).catch(function(error) {
                    reject(error)
                });
            });
           
        },

        obtenerSucursalesPorTradeName : function(tradeName){
            return new Promise(function(resolve, reject){
                common.sqls.connect().then(function () { 
                    var query = "SELECT Code FROM Sucursales WHERE TradeName = '" + tradeName + "'";
                    common.sqls.ejecutarQuery(query).then(function(result) {
                        resolve(result)
                    }).catch(function(error) {
                        reject(error)
                    });
                }).catch(function(error) {
                    reject(error)
                });; 
            });
        },
        obtenerSucursalesAgrupadas : function (){
            return new Promise(function(resolve, reject){
                common.sqls.connect().then(function () { 
                    var query = "SELECT TradeName FROM Sucursales GROUP BY TradeName";
                    common.sqls.ejecutarQuery(query).then(function(result) {
                        resolve(result)
                    }).catch(function(error) {
                        reject(error)
                    });
                }).catch(function(error) {
                    reject(error)
                });; 
            });
        },
        getCSVSucursales    :   function(fileName){
            let sucursales = [],
                count = 0;
            return new Promise(function(resolve, reject){

                try{
                    fcsv.fromPath(config.fileCSVSucursales)
                    .on("data", function(data){
                        
                        let code = data[0].trim(),
                            sucursal = { 'code' :  code };

                        if(code && count != 0) //el primero no va, es el nombre de la columna
                            sucursales.push(sucursal);
                        count++;
                    })
                    .on("end", function(){
                        resolve(sucursales);
                    })
                    .on("error", function(err){
                        reject(err);
                    });
                }catch(e){
                    console.log('Error getCSVSucursales')
                    reject(e);
                }
            });
        }
    }
})();
