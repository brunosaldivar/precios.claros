const config = require('./../config.js').Config;
const common = require('./../common').commons;
const TYPES = common.types;

exports.categorias =(function (){
    
    return {
        createTable  : function(){

            return new Promise(function(resolve, reject){
                
                common.files.open(config.dbFiles.CREATE_TABLE_pc_categorias).then(function(query){
                    common.sqls.ejecutarQuery(query).then(function(result) {
                        resolve()
                    }).catch(function(error) {
                        common.logger.debug({data: error}, true);
                        reject(error)
                    });
                }).catch(function(error) {
                    common.logger.debug({data: error}, true);
                    reject(error)
                });
                
            });
        },
        seed  : function(){

            return new Promise(function(resolve, reject){
                
                common.files.open(config.dbFiles.INSERT_pc_categorias).then(function(query){
                    common.sqls.ejecutarQuery(query).then(function(result) {
                        resolve()
                    }).catch(function(error) {
                        common.logger.debug({data: error}, true);
                        reject(error)
                    });
                }).catch(function(error) {
                    common.logger.debug({data: error}, true);
                    reject(error)
                });
                
            });
        },
        setCategoryFinishParallelProcess : function(code){

            return new Promise(function(resolve, reject){
                 
                let query = "UPDATE pc_categorias SET isExcluded = 0, finishExec = GETDATE() WHERE code = '" + code + "'";
                common.sqls.ejecutarQuery(query).then(function(result) {
                    
                    resolve(result)
                }).catch(function(error) {
                    reject(error)
                });
            });
           
        },
        //PRIMERO SETEAR EN 1 CDO TERMINA EN 0
        setCategoryInitParallelProcess : function(codes){

            return new Promise(function(resolve, reject){
                 
                let query = "UPDATE pc_categorias SET isExcluded = 1, initExec = GETDATE() WHERE code IN (" + codes + ") ";
                common.sqls.ejecutarQuery(query).then(function(result) {
                    
                    resolve(result)
                }).catch(function(error) {
                    reject(error)
                });
            });
           
        },
        getCategoriasParallel : function(categoriesExecuted){
            return new Promise(function(resolve, reject){
                 
                let query = '';
                if(categoriesExecuted){
                    
                    query = "SELECT TOP " + config.parallelProcess  + " code FROM pc_categorias WHERE isExcluded = 0 AND code NOT IN (" + categoriesExecuted + ")";
                }
                else
                {
                    query = "SELECT TOP " + config.parallelProcess  + " code FROM pc_categorias WHERE isExcluded = 0";
                }    
                common.sqls.ejecutarQuery(query).then(function(result) {
                    
                    resolve(result)
                }).catch(function(error) {
                    reject(error)
                });
            });
           
        },
        getCategorias : function(){
            return new Promise(function(resolve, reject){
                 
                var query = "SELECT code, name FROM pc_categorias WHERE isExcluded = 0";
                common.sqls.ejecutarQuery(query).then(function(result) {
                    
                    resolve(result)
                }).catch(function(error) {
                    reject(error)
                });
            });
           
        },
    }
})();