const config = require('./../config.js').Config;
const common = require('./../common').commons;
const request = require('request');
const TYPES = common.types;
const sleep = require('system-sleep');

exports.productos  = (function (){
   
    
	return {
        truncate  : function(){

            return new Promise(function(resolve, reject){
                
                common.sqls.ejecutarQuery('TRUNCATE TABLE dbo.pc_productos').then(function(result) {
                    resolve()
                }).catch(function(error) {
                    common.logger.debug({data: error}, true);
                    reject(error)
                });
            
            });
        },
        createTableHistory  : function(){

            return new Promise(function(resolve, reject){
                
                common.files.open(config.dbFiles.CREATE_TABLE_pc_productos_history).then(function(query){
                    common.sqls.ejecutarQuery(query).then(function(result) {
                        resolve()
                    }).catch(function(error) {
                        common.logger.debug({data: error}, true);
                        reject(error)
                    });
                }).catch(function(error) {
                    common.logger.debug({data: error}, true);
                    reject(error)
                });
                
            });
        },
        insertIntoHistory  : function(){

            return new Promise(function(resolve, reject){
                
                common.files.open(config.dbFiles.INSERT_INTO_pc_productos_history).then(function(query){
                    common.sqls.ejecutarQuery(query).then(function(result) {
                        resolve()
                    }).catch(function(error) {
                        common.logger.debug({data: error}, true);
                        reject(error)
                    });
                }).catch(function(error) {
                    common.logger.debug({data: error}, true);
                    reject(error)
                });
                
            });
        },
        createTable  : function(){

            return new Promise(function(resolve, reject){
                
                common.files.open(config.dbFiles.CREATE_TABLE_pc_productos).then(function(query){
                    common.sqls.ejecutarQuery(query).then(function(result) {
                        resolve()
                    }).catch(function(error) {
                        common.logger.debug({data: error}, true);
                        reject(error)
                    });
                }).catch(function(error) {
                    common.logger.debug({data: error}, true);
                    reject(error)
                });
                
            });
        },
        
        bulkInsert : function (productos, sucursal, categoria){
            return new Promise(function(resolve, reject){

                common.sqls.connect().then(function(connection){
                    var bulkLoad = connection.newBulkLoad('dbo.pc_productos', function (error, rowCount) {
                        if(error){
                            common.logger.log({data : error} ,true);
                            connection.close();
                            reject(error)
                        }else{
                            common.logger.log({msg: '['+ categoria +'] ' + rowCount  + ' rows inserted' });
                            connection.close();

                            if(rowCount <= 1000 && rowCount != 0 ){
                                common.logger.log({msg: '['+ categoria +'] sleeping'})
                                sleep(config.timeToSleep)
                                common.logger.log({msg: '['+ categoria +'] returned'})
                            }
                            resolve();
                        }
                    });
                    //bulkLoad.addColumn('id', TYPES.Int, { length: 100, nullable: true});
                    bulkLoad.addColumn('code', TYPES.NVarChar, { length: 100, nullable: true});
                    bulkLoad.addColumn('marca', TYPES.NVarChar, { length: 50, nullable: true});
                    bulkLoad.addColumn('precioMax', TYPES.Decimal,{ precision: 18, nullable: true});
                    bulkLoad.addColumn('precioMin', TYPES.Decimal,{ precision: 18, nullable: true});
                    bulkLoad.addColumn('nombre', TYPES.NVarChar, { length: 150, nullable: true});
                    bulkLoad.addColumn('presentacion', TYPES.NVarChar, { length: 50, nullable: true});
                    bulkLoad.addColumn('sucursalCode', TYPES.NVarChar, { length: 50, nullable: true});
                    bulkLoad.addColumn('fechaAlta', TYPES.DateTime, { nullable: true}); 
                    bulkLoad.addColumn('categoriaCode', TYPES.NVarChar, { length: 50, nullable: true});
                    //console.log(sucursales);
                    for (i = 0; i <= productos.length ; i++){
                        if(productos[i]){
                            productos[i].code = productos[i].id;
                            delete productos[i].id;
                            productos[i].fechaAlta = new Date();
                            productos[i].sucursalCode = sucursal.trim();
                            productos[i].categoriaCode = categoria.trim();
                            bulkLoad.addRow(productos[i]);
                        }
                    }
                    connection.execBulkLoad(bulkLoad);
                    
                    }).catch(reject);
            });
        },
        /* Primer fc de prueba con arreglos */
        obtenerProductosPorCategoriaArraySucursal : function (categoria, sucursales){
            return new Promise(function(resolve, reject){

                var options = {
                    url: config.url.productos,
                    method: 'GET',
                    qs: 
                    { 
                        id_categoria: categoria, // '05',
                        array_sucursales: sucursales,
                        offset: '0',
                        limit: '50',
                        sort: '-cant_sucursales_disponible' 
                    },
                    headers: {
                        'x-api-key':config.key,
                    },
                };
                request(options, function(error,response, html){
                        if (!error){

                            resolve(null, response,html);

                        }else{
                            common.logger.log({data : error },true); 
                            reject(error,null);
                        }
                    });  
            });
        },
        obtenerProductosPorCategoriaSucursal : function (opt){
            
            return new Promise(function(resolve, reject){

                if(!opt.sort)
                    opt.sort    = '-cant_sucursales_disponible';

                var options = {
                    url: config.url.productos,
                    method: 'GET',
                    qs: 
                    { 
                        id_categoria: opt.categoria, // '05',
                        id_sucursal : opt.sucursal.trim(),
                        offset: opt.offset,
                        limit: opt.limit,
                        sort: opt.sort 
                    },
                    headers: {
                        'x-api-key':config.key,
                    },
                };
                request(options, function(error,response, html){
                    
                   
                    if (!error){ 
                        resolve(response);
                     }else{
                        common.logger.log({data: error}, true);
                        reject(error);
                    }
                });  
            });
        }
    }
})();
