const   common = require('../common').commons,
        config = require('./../config.js').Config,
        TYPES = common.types;
        
exports.sucursales_compare =(function (){
    
    return {
        truncate : function(){
            return new Promise(function(resolve, reject){
                let query = "TRUNCATE TABLE pc_sucursales_compare";
                common.sqls.ejecutarQuery(query).then(function(result) {
                    resolve(result)
                }).catch(function(error) {
                    common.logger.debug({data: error}, true);
                    reject(error)
                });
            });
           
        },
        createTable  : function(){

            return new Promise(function(resolve, reject){
                
                common.files.open(config.dbFiles.CREATE_TABLE_pc_sucursales_compare).then(function(query){
                    common.sqls.ejecutarQuery(query).then(function(result) {
                        resolve()
                    }).catch(function(error) {
                        common.logger.debug({data: error}, true);
                        reject(error)
                    });
                }).catch(function(error) {
                    common.logger.debug({data: error}, true);
                    reject(error)
                });
                
            });
        },
        bulkInsert : function (sucursales){
            return new Promise(function(resolve, reject){
                common.sqls.connect().then(function(connection){
                    var bulkLoad = connection.newBulkLoad('dbo.pc_sucursales_compare', function (error, rowCount) {
                        if(error){
                            common.logger.log({data : error} ,true);
                            connection.close();
                            reject(error)
                        }else{
                            common.logger.log({msg:'[pc_sucursales_compare] ' + rowCount + ' rows inserted' });
                            connection.close();
                            resolve();
                        }
                    });
                    bulkLoad.addColumn('code', TYPES.NVarChar, { length:50, nullable: true});

                    for (i = 0; i <= sucursales.length -1 ; i++){
                        //common.logger.log({data:sucursales[i]},true);
                        bulkLoad.addRow(sucursales[i]);
                    }
                    connection.execBulkLoad(bulkLoad);
                    
                    }).catch(reject);
            });
        },
        sync: function (){

            return new Promise(function(resolve, reject){
                let query = "exec pc_sucursales_sync";
                common.sqls.ejecutarQuery(query).then(function(result) {
                    resolve(result)
                }).catch(function(error) {
                    reject(error)
                });
            });


        },
    }
})();