const   config = require('./config').Config,
        managers = require('./managers/').managers,
        models = require('./models').models,
        common = require('./common').commons,
        bootstrap = require('./bootstrap').bootstrap,
        cron = require("node-cron");

        sequential = {
            categoriesExecuted  :     '',
            count               :     1,
            exec                :   (executed) =>   {

                return new Promise(function(resolve,reject){

                    managers.parallel.exec(executed).then((categoriesExec)=>{
                        
                        sequential.categoriesExecuted += ',' + categoriesExec;
                        if(sequential.categoriesExecuted.charAt(0) == ',')
                            sequential.categoriesExecuted = sequential.categoriesExecuted.substring(1);

                        if(sequential.count < config.sequentialCount){
                            sequential.count++;
                            sequential.exec(sequential.categoriesExecuted).then(resolve).catch(reject);
                         }else{
                           
                            resolve();
                        }
                    }).catch((e) => {
                        reject(e);
                    });
                });
            }
        };

if(process.argv.length == 0 ||process.argv.length < 3  ){
    console.log(config.modoUso);
    process.exit()
}
process.argv.forEach(function (val, index, array) {
    switch  (val){
        case '-h':   
            console.log(config.modoUso);
            process.exit();
            break;
        case '-help':    
            console.log(config.modoUso);
            process.exit();
            break;
        case '-csv':
            managers.sucursales.CSVSync().then(function(){
                process.exit();
            }).catch(function(e){
                process.exit();
            });
            break;

            /* Creates tables and seed data */
        case '-i':
                bootstrap.init().then(function(){
                    common.logger.log({msg: '[init] complete'});
                    process.exit();
                }).catch((e) => {
                    common.logger.log({data: e}, true); 
                    process.exit();
                });    
            
            break;
        
        /* process category */
        case '-a':

            var categoria  =array[index +1 ];
            managers.productos.procesar(categoria).then(function(){
                common.logger.log({msg: '['  + categoria + '] resolved' });
                //process.exit(); 
            }).catch(function(e){
                common.logger.log({data: e}, true);
                process.exit();
            });
          
            break;
        case '-p':


            managers.parallel.exec().then(()=>{
                console.log('Process finished!');
                process.exit();
            }).catch((e) => {
                common.logger.log({data: e}, true);
                process.exit();
            });
            break;

        /* Secuencial y paralelo:
            paraleliza: X categorias
            cuando termina sigue con X  */

        case '-s':

            managers.productos.prepare().then(()=> {
                sequential.exec().then(()=>{
                    common.logger.log('Process finished!');
                    process.exit();
                }).catch((e) => {
                    common.logger.log({data: e}, true);
                    process.exit();
                });
            }).catch((e) => {
                common.logger.log({data: e}, true);
                process.exit();
            });
            
            break;

        /** Igual que -s pero modo job */
        case  '-j':
          
            cron.schedule("" + config.jobMinutes + " " + config.jobHour + " * * *", () =>  {
                
                console.log("Running job..");
                managers.productos.prepare().then(()=> {
                    sequential.exec().then(()=>{
                        common.logger.log('Process finished!');
                        process.exit();
                    }).catch((e) => {
                        common.logger.log({data: e}, true);
                        process.exit();
                    });
                }).catch((e) => {
                    common.logger.log({data: e}, true);
                    process.exit();
                });
            });
            break;
    }
});