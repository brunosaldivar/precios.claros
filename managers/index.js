const sucursales = require('./sucursales.js').sucursales;
const productos = require('./productos.js').productos;
const parallel = require('./parallel.js').parallel;
exports.managers  = {productos: productos, sucursales: sucursales, parallel: parallel};