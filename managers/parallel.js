const child_process = require('child_process'),
    sleep = require('system-sleep'),
    models = require('../models').models,
    config = require('../config').Config,
    parent = {
        requestCount : 0,
        categoryCount : 0,
        total:  1,
        child_category: function(category){
            return new Promise(function(resolve,reject){
            
                try{
                    const child = child_process.fork('./main.js', ['-a', category]);
                    child.on('message', function(data) {
                        console.log(data)
                    });
                    child.on('exit', function() {
                        
                        console.log('[' + category +'] exit')

                        models.categorias.setCategoryFinishParallelProcess(category).then(()=>{
                            resolve();
                        }).catch(reject); 
                        
                    });
                }catch(e){

                    reject(e);
                }
            });
        }
    };

exports.parallel =(function (){
   
    return {
       
        exec    : (categoriesExecuted) => {
            let categoryCount = 0;
            console.log('categoriesExecuted: ' + categoriesExecuted);
            return new Promise(function(resolve,reject){
                
                models.categorias.getCategoriasParallel(categoriesExecuted).then((categorias) =>{
                    console.log('categorias: ' + categorias.length);
                    
                    let codesToString = '';
                    
                    categorias.forEach(category => {
                        codesToString+= ",'" + category[0].value.trim() + "'";
                    });
                    codesToString =codesToString.substring(1);
                                        
                    parent.total = categorias.length;
                    models.categorias.setCategoryInitParallelProcess(codesToString).then(()=>{
                        categorias.forEach(category => {
                            let catego = category[0].value.trim();
                            parent.child_category(catego).then(function(){
                                
                                categoryCount++
                                if(categoryCount == parent.total){
                                    //en caso de ser paralelo y sequencial, paso las categorias que ya procesé
                                    resolve(codesToString);
                                }
                             
                            }).catch(reject); 
                        });
                    }).catch(reject);
           
                }).catch(reject);

            });

        },
    }
})();
