var models = require('../models').models;
var common = require('../common').commons;
var config = require('../config').Config;

exports.sucursales = (function(){
    
    optSucursal = {
        offset      :   ( !this.offset ? 0: this.offset),
        limit       :   30,                     //"maxLimitPermitido": 30
        count       :   0,
        sucursales   :   []
    };
    
    procesarSucursal      =  function(){
        
        return new Promise(function(resolve, reject){
            //common.logger.log({data: optSucursal},true);
            models.sucursales.obtenerSucursales(optSucursal)
                .then(function(response){

                    if(response.statusCode && response.statusCode == 200) {
                        if(response.body.errorMessage)
                        {
                            common.logger.log({msg: '[Server] errorMessage: ' + response.body.errorMessage});
                            reject(response.body.errorMessage);
                        }
                        let jsonSucursales = JSON.parse(response.body);
                        optSucursal.offset += jsonSucursales.maxLimitPermitido;
                        //API dice que hay más pero puede no traer nada, salgo
                        if(jsonSucursales.sucursales.length != 0){
                            optSucursal.count +=  jsonSucursales.sucursales.length;
                            optSucursal.sucursales.pushArray( jsonSucursales.sucursales)
                            if(jsonSucursales.total  == optSucursal.count){
                                resolve(optSucursal.sucursales);
                            }else{
                                procesarSucursal().then(resolve).catch(reject);
                            }
                        }else{
                            //common.logger.log({msg:optSucursal.sucursales.length  + ' sucursales resueltas'});    
                            resolve(optSucursal.sucursales);
                        }
                    }else{

                        common.logger.log({msg: response.request.uri.href})
                        common.logger.log({msg:'statusCode: ' + response.statusCode});
                        reject();
                    }

                }).catch(reject); 
        });

    };
    return {
        
      
        procesar : function(){
            
            return new Promise(function(resolve, reject){
                try{
                    procesarSucursal().then(function(sucursales){
                        models.sucursales.insertMasivo(sucursales).then(function(){
                            resolve();
                        }).catch(function(error){
                            common.logger.debug({data: error}, true);
                            reject(error)
                        });
                    }).catch(reject);
                
                }catch(ex){
                    reject(ex)
                }
        });},
        CSVSync :   () => {

            return new Promise(function(resolve, reject){        
                common.logger.log({msg: '[seed] sucursales.CSVSync()..'})

                models.sucursales_compare.truncate().then(()=>{ 
                    ///models.sucursales.truncate().then(()=>{

                    models.sucursales.getCSVSucursales().then((codes)=>{

                        common.logger.log({msg: '[seed] sucursales.bulkInsert()..'})

                        models.sucursales_compare.bulkInsert(codes).then(function(){

                            models.sucursales_compare.sync().then(function(){
                                common.logger.log({msg: '[seed] sucursales_compare.sync() complete'})
                                resolve();
                            }).catch(function(e){
                                common.logger.log({msg: '[seed] error sucursales_compare.sync()'})
                                common.logger.log({data: e}, true)
                            })
                        }).catch(function(e){
                            common.logger.log({msg: '[seed] error sucursales_compare.bulkInsert()'})
                            common.logger.log({data: e}, true)
                        });

                    }).catch((error)=>{
                        common.logger.log({msg: 'error sucursales.getCSVSucursales()'});
                        common.logger.debug({data: error}, true);
                        reject(error)
                    });
              /*   }).catch((error)=>{
                    common.logger.log({msg: 'error sucursales.truncate()'});
                    common.logger.debug({data: error}, true);
                    reject(error)
                }); */
            }).catch((error)=>{
                common.logger.log({msg: 'error sucursales_compare.truncate()'});
                common.logger.debug({data: error}, true);
                reject(error)
            });
        })},
        /*DEPRECATED:  */
        importarSucursalesCSV   :   function(){

            return new Promise(function(resolve, reject){

                models.sucursales.getCSVSucursales().then(function(sucursales){

                    models.sucursales.insertMasivo(sucursales).then(function(){
                        resolve();
                    }).catch(function(err){
                        console.log('error sucursales.insertMasivo')
                        common.logger.debug({data: err}, true);
                        reject(err);
                    });
                }).catch(function(err){
                    console.log('error sucursales.insertMasivo()')
                    common.logger.debug({data: err}, true);
                    reject(err);
                });
            });
        },

    };
})();