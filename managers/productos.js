const models = require('../models').models;
const commons = require('../common').commons;
const config = require('../config').Config;
const sleep = require('system-sleep');

Array.prototype.pushArray = function(arr) {
    this.push.apply(this, arr);
};

exports.productos = (function(){

    optSucursales = {
        count   : 0,    
        total   : 0
    };
    let _categoria = '';
    /*
        Obtiene los productos de toda la sucursal por categoria
    */
    procesarProducto    =   function(suc){
        optSucursal.sucursal = suc;

        return new Promise(function(resolve, reject){
            
            models.productos.obtenerProductosPorCategoriaSucursal(optSucursal)
                .then(function(response){

                if(response.statusCode &&  response.statusCode == 200) {
                    if(response.body.errorMessage)
                    {
                        commons.logger.log({msg: '[Server] errorMessage :' + response.body.errorMessage});
                        reject(response.body.errorMessage);
                    }

                    let jsonPrecios = JSON.parse(response.body);
                    optSucursal.offset += jsonPrecios.maxLimitPermitido;
                    optSucursal.count += jsonPrecios.productos.length;
                    optSucursal.productos.pushArray( jsonPrecios.productos)

                    if(jsonPrecios.total  == optSucursal.count){
                        resolve(optSucursal);
                    }else{
                        procesarProducto(suc).then(resolve).catch(reject);
                    }
                }else{

                    commons.logger.log({msg: response.request.uri.href})
                    commons.logger.log({msg:'statusCode: ' + response.statusCode});
                    reject();
                }
            }).catch(reject); 
           
        });

    };
    procesarProductos = function(sucs){
        
        optSucursales.count++;
        optSucursales.total = sucs.length;
        optSucursal = {
            categoria   :   _categoria,                      
            sucursal    :    '',
            offset      :   ( !this.offset ? 0: this.offset),
            limit       :   100,                     //"maxLimitPermitido": 100
            sort        :   '+descripcion',
            count       :   0,
            productos   :   []
        };
        return new Promise(function(resolve, reject){
            
            var ind = optSucursales.count  -1;
            //let codeSuc= sucs[ind][0]['value'].trim() 
           // commons.logger.log({msg: '['+ _categoria +'] sucursal: ' + sucs[ind][1]['value'].trim() + ' (' + codeSuc   + ')'});
           
            procesarProducto(sucs[ind][0]['value']).then(function(sucursalCompleta){

                if(sucursalCompleta.productos == 0){
                    commons.logger.log({msg: '['+ _categoria +'] sleeping'})
                    sleep(config.timeToSleep)
                    commons.logger.log({msg: '['+ _categoria +'] returned'})
                }
                
                models.productos.bulkInsert(sucursalCompleta.productos, sucs[ind][0]['value'], _categoria).then(function(){
                    
                    if(optSucursales.count < optSucursales.total ){

                        commons.logger.log({msg:'['+ _categoria +'] ' + optSucursales.count + ' of ' +optSucursales.total});
                        
                        return procesarProductos(sucs).then(resolve).catch(reject)

                    }else{

                        resolve(sucs);

                    }
                }).catch(function(error){
                    reject(error)
                });
                
            }).catch(function(err){
                //console.log(err);
                reject(err)
            });
        });
    };
    return { 
        prepare    :  function(){
            return new Promise(function(resolve, reject){
                commons.logger.log({msg: '[prepare] productos.insertIntoHistory()..'});
                models.productos.insertIntoHistory().then(
                    () => {
                        commons.logger.log({msg: '[prepare] productos.truncate()..'});
                        models.productos.truncate().then(
                            () => {
                                resolve();
                            }).catch((e)=> {
                                commons.logger.log({msg: 'error productos.truncate()'});
                                reject()
                            });
                    }).catch((e)=> {
                        commons.logger.log({msg: 'error productos.truncate()'});
                        reject()
                    });
            });
        },   
        setCategoria   :  function (categ){
            _categoria = categ;
        },
        procesar    :  function(categoria){
            _categoria = categoria;
            return new Promise(function(resolve, reject){
                
                //levantar config de tabla con las sucs ya obtenidas
                models.sucursales.getSucursalesDB().then( function( sucs ) {
             
                    commons.logger.log( {msg: sucs.length  + ' sucursales obtenidas'});
             
                    procesarProductos(sucs).then(function(sucs){
                        commons.logger.log({msg: sucs.length  + ' sucursales guardadas'});
                        resolve()

                    }).catch(function(error){
                        commons.logger.log('error procesarProductos()'); 
                        reject(error);
                    });

                }).catch(function(error){
                    commons.logger.log('error sucursales.getSucursalesDB()'); 
                    reject(error);
                });
            });
        },
    }
})();

