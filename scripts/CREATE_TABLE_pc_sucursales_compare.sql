
IF (EXISTS 
        (SELECT * 
            FROM INFORMATION_SCHEMA.TABLES 
            WHERE TABLE_SCHEMA = 'dbo'
            AND  TABLE_NAME = 'pc_sucursales_compare')
    )
    BEGIN 
        DROP TABLE [dbo].[pc_sucursales_compare]
    END

CREATE TABLE [dbo].[pc_sucursales_compare](
    [code] [nvarchar](50) NULL
) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [IX_code_compare] ON [dbo].[pc_sucursales_compare]
(
    [code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

    


