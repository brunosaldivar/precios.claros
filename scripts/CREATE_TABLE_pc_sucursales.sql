IF (EXISTS (SELECT *  
                    FROM INFORMATION_SCHEMA.TABLES  
                    WHERE TABLE_SCHEMA = 'dbo' 
                    AND  TABLE_NAME = 'pc_sucursales'))  
BEGIN   
    DROP TABLE [dbo].[pc_sucursales]
END

CREATE TABLE [dbo].[pc_sucursales]( 
        [id] [int] IDENTITY(1,1) NOT NULL, 
        [code] [nvarchar](50) NOT NULL, 
        [banderaId] [int] NOT NULL, 
        [sucursalNombre] [nvarchar](100) NOT NULL,  
        [sucursalTipo] [nvarchar](100) NOT NULL,  
        [provincia] [nvarchar](50) NOT NULL,  
        [direccion] [nvarchar](150) NOT NULL,  
        [banderaDescripcion] [nvarchar](150) NOT NULL,  
        [localidad] [nvarchar](100) NOT NULL,  
        [comercioRazonSocial] [nvarchar](150) NOT NULL,  
        [comercioId] [int] NOT NULL, 
        [sucursalId] [int] NOT NULL, 
        [isExcluded] [bit] NULL, 
        ) ON [PRIMARY]   
ALTER TABLE [dbo].[pc_sucursales] ADD  CONSTRAINT [PK_pc_sucursales] PRIMARY KEY CLUSTERED ( 
    [id] ASC  
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]  
ALTER TABLE [dbo].[pc_sucursales] ADD  CONSTRAINT  DF_isExcluded default(1) FOR isExcluded  
CREATE INDEX ix_pc_sucursales_code ON pc_sucursales (code) 