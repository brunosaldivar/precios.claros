IF (EXISTS (SELECT *  
                    FROM INFORMATION_SCHEMA.TABLES  
                    WHERE TABLE_SCHEMA = 'dbo' 
                    AND  TABLE_NAME = 'pc_categorias'))  
BEGIN   
    DROP TABLE [dbo].[pc_categorias]
END

CREATE TABLE [dbo].[pc_categorias](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [nchar](10) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[initExec] [datetime] NULL,
	[finishExec] [datetime] NULL,
	[isExcluded] [bit] NULL,
CONSTRAINT [PK_pc_Categorias] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
