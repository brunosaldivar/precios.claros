IF NOT EXISTS (SELECT * FROM sysobjects where name = 'pc_sucursales_sync')
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE pc_sucursales_sync AS
        UPDATE pc_sucursales
        SET isExcluded = 0 
        WHERE code IN (	SELECT code FROM pc_sucursales_compare)'
END

