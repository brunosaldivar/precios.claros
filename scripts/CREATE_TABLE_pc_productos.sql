IF (EXISTS (SELECT *  
                    FROM INFORMATION_SCHEMA.TABLES  
                    WHERE TABLE_SCHEMA = 'dbo' 
                    AND  TABLE_NAME = 'pc_productos'))  
BEGIN   
    DROP  TABLE [dbo].[pc_productos]
END

CREATE TABLE [dbo].[pc_productos](
    [id] [int] IDENTITY(1,1) NOT NULL,
    [code] [nvarchar](100) NULL,
    [marca] [nvarchar](50) NULL,
    [precioMax] [decimal](18, 0) NULL,
    [precioMin] [decimal](18, 0) NULL,
    [nombre] [nvarchar](100) NULL,
    [presentacion] [nvarchar](50) NULL,
    [sucursalCode] [nvarchar](50) NULL,
    [fechaAlta] [datetime] NULL,
    [categoriaCode] [nvarchar](50) NULL,
CONSTRAINT [PK_pc_productos] PRIMARY KEY CLUSTERED 
(
    [id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
    
CREATE INDEX ix_pc_productos_categoriaCode ON pc_productos (categoriaCode) 
    
CREATE INDEX ix_pc_productos_sucursalCode ON pc_productos (sucursalCode) 
    
CREATE INDEX ix_pc_productos_categoriaCode_sucursalCode ON pc_productos (categoriaCode, sucursalCode) 
    
