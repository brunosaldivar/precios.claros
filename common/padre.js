const child_process = require('child_process');

models.categorias.obtenerCategorias().then(function(categorias){
    console.log('categorias: ' + categorias.length);
    categorias.forEach( (catego) => {

        let categoria = catego[0].value.trim();    
        const child = child_process.fork('./main.js', ['-a', categoria]);

        child.on('message', function(message) {
            console.log(message)
        });
    });

}).catch(function(e){
    console.log(e)
    process.exit();
});

