var fcsv = require("fast-csv");
var config = require('../config').Config;

exports.csv = (function(){
    sucursales              =   [];
    return {
        
        getCSVSucursales    :   function(fileName){
            
            return new Promise(function(resolve, reject){

                try{
                fcsv.fromPath(config.fileSucursales)
                    .on("data", function(data){
                        
                        var s0 = data[0].replace('/', '-');
                        var code = s0.replace('/', '-');
                        var name = data[1].trim();
                        var tradeName = data[2].trim();

                        var sucursal = { 'Code' :  code, 'Name' :   name, 'TradeName':   tradeName };
                        if(code)
                            sucursales.push(sucursal);
                    })
                    .on("end", function(){
                        
                        resolve(sucursales);
                    })
                    .on("error", function(err){
                        reject(err);
                    });
                }catch(e){
                    console.log('Error en obtenerCSVSucursales')
                    reject(err);

                }
            });
        },

    };
})();