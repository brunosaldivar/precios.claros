const fs = require('fs');

exports.files = (function(){
    return {

    open    :   function (filename){
        return new Promise(function(resolve, reject){
        fs.readFile(filename, 'utf8', function(err, data) {
            if (err) reject(err);
            resolve(data);
          });
        });
        }
    };
})();
