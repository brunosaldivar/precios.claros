const csv = require('./csv.js').csv;
const sqls = require('./db.js').sqlEngine;
const logger = require('./log.js').logger;
const files = require('./files.js').files;

exports.commons = {cvs: csv, sqls: sqls, types: sqls.TYPES, logger : logger, files: files};