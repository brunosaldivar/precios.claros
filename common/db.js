const Connection = require('tedious').Connection;
const Request = require('tedious').Request;
const TYPES = require('tedious').TYPES;
const config = require('../config').Config;

exports.sqlEngine = (function(){

    function connect() {
        return new Promise(function(resolve, reject){

            var connection = new Connection(config.db);
            connection.on('end', function(err) 
            {
              //  console.log('desconectado')
            });
            connection.on('connect', function(err) 
            {
                //console.log('err');
                if (err) 
                {
                    console.log(err);
                    reject(err);
                }else   {
                    //console.log('err');
                    resolve(connection);
                }     
            });
            
        });
        
    };
    return {
        TYPES       : TYPES,
        connect               : function(){
            return new Promise(function(resolve, reject){
    
                var connection = new Connection(config.db);
                connection.on('end', function(err) 
                {
                    //console.log('desconectado')
                });
                connection.on('connect', function(err) 
                {
                    if (err) 
                    {
                        console.log(err);
                        reject(err);
                    }else   {
                        resolve(connection);
                    }     
                });
                
            });
            
        },
        ejecutarQuery : function (query, callback){
            
            return new Promise(function(resolve, reject){
                connect().then(function(connection){
                    var results = [];
                    
                    request = new Request(query, function(err, rowCount,rows) {
                        
                        if (err) {
                            reject(err);
                            connection.close();
                        }else{
                            connection.close();
                            resolve(results);
                        }
                    });
                    request.on("row", function(rowObject) {
                        results.push(rowObject);
                    });
                    connection.execSql(request);

                    /* var request = new Request(query, function(error) {
                        
                        if (error) {
                            reject(error);
                            connection.close();
                        }else{
                            connection.close();
                            resolve(results);
                        }
                    });
                        
                    request.on("row", function(rowObject) {
                        results.push(rowObject);
                    });
                    connection.execSql(request); */
                });
            })
        }
    };
})();