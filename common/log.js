
const config = require('../config').Config;

exports.logger = (function(){

    return {

        /**
         * obMsg = {
         *          db      :   db log
         *          msg     :   string message
         *          data    :   objects {}
         *          req     : requestLimiter msg
         *          date    :   ?
         * }
         */
        log     :   function (obMsg,dir){

            
            if(dir){
                console.dir( obMsg.data);
            }
            else{
                if (config.parallel){
            
                    process.send = process.send || function () {};
                    process.send( obMsg.msg +( obMsg.data ? obMsg.data : ''));

                  /*   if(obMsg.req){
                        process.send('requesLimiter',
                    } */
                }else{
                    console.log( obMsg.msg +( obMsg.data ? obMsg.data : ''));
                }
            }
        },
        debug     :   function (obMsg,dir){
            
            if(config.debug){
                console.dir( obMsg.data);
            }
        }
    };
})();



